/**
To add a new migration script, use the CLI command :
migrate-mongo create name-of-my-script

A new file will be created with a corresponding timestamp.
|_ migrations/
   |- 20210108114324-name-of-my-script.js

To run the migration :
migrate-mongo up
*/
const Config = require('../src/core/config');
const fs = require('fs');

module.exports = {
  async up(db, client) {
    // TODO write your migration here.
    // See https://github.com/seppevs/migrate-mongo/#creating-a-new-migration-script
    // Example:
    // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: true}});

    let localInstance = await db.collection('instances').findOne({ isLocal: true })
    if(localInstance == null || localInstance.name != "alpha"){
      console.log('You are lucky, this migration is only needed on Alpha node, ' + 
                  'because he made a big bullshit on his server.')
      return
    }  

    await db.collection('pages').find()
          .forEach((page) => {
            // console.log( 'COPY FILE :', `${Config.PUBLIC_FOLDER_PATH}/default-avatar.png`, 
            //                             `${Config.PUBLIC_FOLDER_PATH}/uploads/avatar/${page.uid}.png`, )
            fs.copyFile(
              `${Config.PUBLIC_FOLDER_PATH}/default-avatar.png`, 
              `${Config.PUBLIC_FOLDER_PATH}/uploads/avatar/${page.uid}.png`, 
              (err) => {
                  if (err) throw err;
                  console.log(`DEBUG: File copy ok: 'default-avatar.png' -> ${Config.PUBLIC_FOLDER_PATH}/uploads/avatar/${page.uid}.png`);
              })  
          })

    // await db.collection('publications').find({ 'imageName' : { '$exists' : true}})
    //       .forEach((post) => {
    //         console.log( 'DELETE PUBLICATION :', post.imageName, post.uid )  
    //       })
    
    await db.collection('publications').remove({ 'imageName' : { '$exists' : true}})
    
  },

  async down(db, client) {
    // TODO write the statements to rollback your migration (if possible)
    // Example:
    // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: false}});
  }
};

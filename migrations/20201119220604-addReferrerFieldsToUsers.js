module.exports = {
  async up(db, client) {
    // Create referrerUid field if it doesn't exists.
    await db.collection('users')
      .updateMany({
        referrerUid: {
          $exists: false
        }
      }, {
        $set: {
          referrerUid: null,
        }
      });
    
    // Create referredCount field if it doesn't exists.
    await db.collection('users')
      .updateMany({
        referredCount: {
          $exists: false
        }
      }, {
        $set: {
          referredCount: 0,
        },
      });
  },

  async down(db, client) {}
};

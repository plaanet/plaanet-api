module.exports = function(params){

  let styleBtn = "padding:10px; margin-top:10px; background-color:#E95048; color:white; border-radius:4px; text-decoration:none;"

  return "<div style='color:white; background-color:#111d20; text-align:center;'>" +

    "<div style='padding: 20px; max-width:600px; margin: 0 auto'>" +
    
      "<a href='https://plaanet.io' target='_blank'>" +
        "<img src='https://plaanet.io/logo-earth.png' height='80'>"+
      "</a>" +

      "<h3>Bonjour " + params.name + ",</h3>" + 

      "<p>" +
        "Vous avez " + params.nbNotifTotal + " notification" + ((params.nbNotifTotal > 1) ? "s" : "") + " en attente" +
        "<br>sur <a href='https://plaanet.io' target='_blank' style='color:#75a6aa'>https://plaanet.io</a>" +
      "</p>" + 

      "<br>" +
      "<a href='https://plaanet.io/stream' target='_blank' style='"+styleBtn+"'>"+
        "Lire mes notifications"+
      "</a>" +

      //"<br>" + params.nbLiveMsgNotRead + " " + params.nbNotif +

      "<br><br><br>" +

      "<a href='https://plaanet.io/stream' target='_blank'>" +
        "<img src='https://plaanet.io/og-cover-image.png' "+
             "style='width:100%;max-width:500px;border-radius:4px;border: 2px solid #54979c;'>"+
      "</a>" +

    "</div>" +

  "</div>"
}
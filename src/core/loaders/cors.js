const cors = require('cors');

const Config = require('../config');
const Instance = require('../../models/Instance');

module.exports = async function corsLoader({ expressApp }) {
  // Generate CORS allowed origins
  if (!global.store.allowedOrigins) {
    const instances = await Instance.find().exec();

    // Once for instances domain
    const allowedDomains = instances
      .map(instance => {
        if (!instance.url) {
          return null;
        }
        
        const portIsSSL = Number.parseInt(instance.port) === 443;
        const instanceURL = new URL(instance.url);

        return portIsSSL
          ? `https://${instanceURL.hostname}`
          : `${instanceURL.protocol}//${instanceURL.hostname}:${instance.port}`;
      })
      .filter(url => url != null);
    
    // Once for instances domain
    const allowedPureDomains = instances
      .map(instance => {
        return (instance.url) ? instance.url : null
      })
      .filter(url => url != null);
    
    // Once for instances websockets url
    const allowedSocketDomains = instances
      .map(instance => {
        if (!instance.url) {
          return null;
        }
        
        const instanceURL = new URL(instance.url);

        return `${instanceURL.protocol}//${instanceURL.hostname}:${instance.portSocket}`;
      })
      .filter(url => url != null);
    
    // Once for clients domain
    const allowedClientDomains = instances
      .map(instance => {
        if (!instance.clientUrl) {
          return null;
        }
        
        const portIsSSL = Number.parseInt(instance.clientPort) === 443;
        const instanceURL = new URL(instance.clientUrl.indexOf('http') === 0 ? instance.clientUrl : `http://${instance.clientUrl}`);

        return portIsSSL
          ? `https://${instanceURL.hostname}`
          : `${instanceURL.protocol}//${instanceURL.hostname}:${instance.clientPort}`;
      })
      .filter(url => url != null);
    
    let corsBypassDev = null;

    if (process.env.NODE_ENV !== 'production') {
      try {
        corsBypassDev = Config.CORS_BYPASS;
      } catch (err) {}
    }
    // Use set to remove duplicates.
    global.store.allowedOrigins = [...new Set([
      'http://localhost',
      'http://localhost:8080',
      'https://localhost',
      'http://127.0.0.1',
      'http://127.0.0.1:8080',
      'https://127.0.0.1',
      'https://127.0.0.1:8080',
      //'http://192.168.1.6:8080',
      'https://plaanet.io',
      'https://nominatim.openstreetmap.org',
      ...allowedDomains,
      ...allowedSocketDomains,
      ...allowedClientDomains,
      ...allowedPureDomains,
      ...(corsBypassDev ? Array.isArray(corsBypassDev) != null ? corsBypassDev : [corsBypassDev] : []),
    ])];
    
    console.log("allowedClientDomains ", allowedClientDomains )
    global.store.allowedOrigins.forEach((origin)=>{
      console.log('DEBUG: Allowed origins for CORS:', origin);
    })
  }


  // Use cors middleware
  expressApp.use(cors({
    // Check if origin is a registered Plaanet instance, else deny.
    origin: async (origin, callback) => {
      // console.log(`WARN: Authorized CORS origin1: ${global.store.allowedOrigins} ?`);
      // console.log(`WARN: Authorized CORS origin: ${origin} ?`);
      // console.log('WARN: Authorized CORS origin:', global.store.allowedOrigins.includes(origin));
      if (origin === undefined || global.store.allowedOrigins.includes(origin)) {
        return callback(null, true);
      } else {
        console.log(`WARN: Unauthorized CORS origin: ${origin} !`);
        return callback(new Error('Origin not allowed by CORS!'));
      }
    },
    optionSuccessStatus: 200,
    credentials: true,
    methods: ['OPTIONS', 'HEAD', 'GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
    allowedHeaders: ['X-Auth-Token', 'X-Init-Token', 'Origin', 'X-Requested-With', 'Content-Type', 'Accept'],
  }));
}
module.exports = class AuthenticationError extends Error {

  constructor(i18nKey, message) {
    super(message);

    this.name = 'AuthenticationError';
    this.httpStatusCode = 403;
    this.i18nKey = i18nKey;
  }

}
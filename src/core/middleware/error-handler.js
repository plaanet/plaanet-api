module.exports = () => function (error, req, res, next) {
  if (error.name) {
    //console.log('ERROR: An error occured:', error.name, error.message);
  }

  if (res.headersSent) {
    return next(error);
  }

  return res.status(error.httpStatusCode || 500).json({
    success: false,
    error: {
      name: error.name,
      i18nKey: error.i18nKey || 'errors.unknownError',
      message: error.message,
      details: error.details || undefined,
    },
  });
};
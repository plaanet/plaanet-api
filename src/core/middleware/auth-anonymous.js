const jwt = require("jsonwebtoken");
const config = require("config");

const { User, validate } = require('../../models/User')

module.exports = async function(req, res, next) { 
  next();
  /*
  //get the token from the header if present
  const token = req.headers["x-auth-token"];
  //if no token found, return response (without going to the next middelware)
  if (!token) {
      return res.status(401).send("Access denied. No token provided.");
  }
  try {
    //if can verify the token, set req.user and pass to next middleware
    const decoded = jwt.verify(token, config.get("access_pk"));
    //console.log("decoded", decoded)
    let user = await User.findOne({ id: decoded.id });
    //console.log("user", user)
    if(user.isAdmin != true) 
        return res.status(401).send("Access denied. You are not admin.");

    req.user = decoded;
    next();
  } catch (ex) { //console.log("error", ex)
    //if invalid token
    res.status(400).send("Invalid token.");
  }*/
};
const UserService = require('./user');
const Page = require('../models/Page');
const Live = require('../models/Live');
const Instance = require('../models/Instance');

const { EntityDoesNotExistsError } = require('../core/errors');
const core = require('../core/core');
const axios = require('axios');
const RoomService = require("../services/room");

module.exports = class PageService {

  constructor() {
    this.userService = new UserService();
  }

  async getPageById(id) {
    return Page
      .findById(id)
      .exec();
  }

  async getPageByUid(uid) {
    return Page
      .findOne({ uid })
      .exec();
  }

  async getPageProfile(uid, userPageCoordinates) {
    let page = await Page.findOne({ uid: uid })
                         .populate("owner")
    //console.log("getPageProfile", page)
    /*if(page == null)
        page = await Page.findOne({ slug: uid })
                         .populate("owner")
    */
        
    if(page == null) return null

    if(page.type == "assembly" && userPageCoordinates != null){
        const roomService = new RoomService()
        page.canSendSurvey = roomService.canSurvey(page, userPageCoordinates)
    }
    return page
  }

  async getPageForUser(id) {
    const user = await this.userService.getUserById(id);
    if (!user) {
      throw new EntityDoesNotExistsError('User does not exists.', 1);
    }

    return Page
      .findOne({
        type: "user",
        owner: user._id,
      })
      .exec();
  }

  async getPagesForUser(id) {
    const user = await this.userService.getUserById(id);
    if (!user) {
      throw new EntityDoesNotExistsError('User does not exists.', id);
    }

    return Page
      .find({ owner: user._id })
      .exec();
  }

  async getMemberPages(pageUid){
    //get pages where pageUid is in roles.editors
    //test test test test test test test
    //console.log("-------------------------")
    //console.log("#PageService.getMemberPages(pageUid)", pageUid)
    
    try{ //broadcast query (to each instance of the network)
      let res = null
      await core.broadcaster.post('/broadcast/get-member-pages', 
            { pageUid: pageUid
            }, //params
            function(dataRes){ // == then (after response from all instances)
                //if network sent data
                if(dataRes != null){
                    res = dataRes //.sort((x, y) => { return (x.alert === y.alert)? 0 : x.alert? -1 : 1; })
                }else{
                    console.log("error 1 : dataRes == ", dataRes)
                    res = false
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch")
            }, 'pages') //dataRes[id] to concat for each response after forward
      return res
    }catch(e){
        console.log("error 3")
        return false
    }
  }

  async getBroadPage(pageUid){
    let nodePath = await core.getNodePath(pageUid)
    //console.log("getBroadPage, nodePath ?", nodePath, 'pageUid ?', pageUid)
    try{
      core.broadcaster.init()
      let res = await axios.post(nodePath + '/broadcast/page-profil', 
                                { pageUid : pageUid, userPageCoordinates: null })
      //console.log("getBroadPage",  res.data)
      if(res.data.error == false) return res.data.page
      else return null //or null ? or false ? or error ?
    }catch(e){
      console.log("error getBroadPage", e)
    }
  }

  insertRole(arr, role, identity){
    let found = false
    arr[role].forEach((id, x) => {
      if(id.uid == identity.uid) found = true
    })
    if(!found) arr[role].push(identity)
    return arr
  }

  deleteRole(page, type, role, identity){
    if(role == "admin" && type == 'roles' && page[type][role].length == 1) return page
    let found = -1
    page[type][role].forEach((id, x) => {
      if(id.uid == identity.uid) found = x
    })

    console.log("deleteRole", page.uid, type, role, page[type][role], found)
    if(found > -1) page[type][role].splice(found, 1)

    return page[type]
  }

  deleteRoleTrace(page, type, role, nodeName){
    console.log("deleteRoleTrace", page.uid, type, role, nodeName)
    let regxNodeName =  new RegExp(".*"+nodeName, "i")
    let found = -1
    page[type][role].forEach((id, x) => {
      if(regxNodeName.test(id.uid)) found = x
    })

    console.log("role found", found > -1)
    if(found > -1) page[type][role].splice(found, 1)
    if(found > -1) console.log("role deleted")

    return page[type]
  }

  isRole(arr, role, identity){
    console.log("isROLE ?", arr[role], role, identity)
    let found = false
    arr[role].forEach((id, x) => {
      if(id.uid == identity.uid) found = true
    })    
    return found
  }

  async createLive(pageUidRef){
    let live = new Live()
    live.parentPageUid = pageUidRef
    live.uid = core.getRandUid(core.iName(pageUidRef))
    live.categories = [
        { uid: core.getRandUid(core.iName(pageUidRef)),
          name: 'Général',
          channels: [
            {   uid: core.getRandUid(core.iName(pageUidRef)),
                name: 'Accueil',
                created: new Date(),
                dateLastMsg: new Date(),
                dateLastOpen: []
            }
          ]
        }
    ]
    await live.save()
    console.log("Create New Live : ", live)
    return live
  }

  async findLiveForPage(pageUid, userPageUid, pageType){
    let pageUidRef = this.getPageUidRef(pageUid, userPageUid, pageType)
    /** TODO : check if user is member or friend */
    let nodePath = await core.getNodePath(pageUidRef)
    let iName = await core.iName(pageUidRef)
    let instance = await Instance.findOne({ isLocal: true })
    
    let live = null
    //console.log("is the good instance ?", instance.name, iName, pageUidRef)
    if(instance.name == iName){
      live = await Live.findOne({ 'parentPageUid' : pageUidRef })
      //console.log("found live in local instance ")
    }else{
      let res =  await core.asyncPost(nodePath + '/broadcast/get-live-data', { pageUidRef: pageUidRef })
      //console.log("res ", nodePath + '/broadcast/get-live-data')
      live = res.data.live //await Live.findOne({ 'parentPageUid' : pageUidRef })
    }
    return live
  }


  getPageUidRef(pageUid, userPageUid, pageType){
    let pageUidRef = pageUid
    if(pageType == "user"){
        /** FORMULE DE CREATION DUID POUR LES CONVERSATIONS PRIVEE ENTRE USER */
        if(pageUid < userPageUid) 
             pageUidRef = pageUid + userPageUid
        else pageUidRef = userPageUid + pageUid
        /** FORMULE DE CREATION DUID POUR LES CONVERSATIONS PRIVEE ENTRE USER */
    }
    return pageUidRef
  }

  //construit la requete à executer pour afficher des pages sur la carte principale
  //en fonction des parametres passé dans la requete initiale (req)
  getMapQuery(params){ //appelé par /page/get-map-pages
    let radiusMax = core.radiusMaxPage*1000 // 500000 //300km
    let radius = params.radius != null ? params.radius : radiusMax 
    if(radius > radiusMax) radius = radiusMax

    let query = { 
        '$and' : [{
            'coordinates': {
                '$geoWithin': {
                    '$centerSphere' :
                        [[params.lng, params.lat], 
                        radius/1000/6378]
                } 
            }
        }]
    }

    let search = params.search
    //console.log("params.search", search)
    
    if(search != "" 
    && search != null
    && search.indexOf("#") == -1){
        query['$or'] = []
        query['$or'].push({ name: new RegExp(".*"+params.search.toLowerCase().trim(), "i") })
    }

    if(search != "" && search != null){
        let words = search.split(' ')
        //console.log("search.split", search, words)
        let arrayW = []
        words.forEach((w)=>{
            let ww = w.trim()
            //if no # then add #
            //console.log("ww.indexOf('#')", ww.indexOf('#'))
            ww = (ww.indexOf('#') === -1) ? "#" + ww : ww
            if(ww != "") arrayW.push(new RegExp(".*"+ww, "i"))
        })
        if(query['$or'] == null) query['$or'] = []
        query['$or'].push({ hashtags : {  '$in' : arrayW  } })

        //console.log("# search.split query hashtags", arrayW)
    }
    
    if(params.pageType != "" && params.pageType != null)
        query['$and'].push({ type: params.pageType })

    query['$and'].push({ uid: { '$ne' : params.pageUid }})
    
    //console.log("query", query, params.nbPerPage)
    let nbPerPage = params.nbPerPage ? params.nbPerPage : 50
    if(nbPerPage > 100) nbPerPage = 100 


    let res = { query: query, 
                limit: nbPerPage, 
                skip: params.skip,
                sort: { 'dateConnected': -1, 'owner.lastActivityDate' : -1 },
                populate: { 
                    path: 'owner',
                    select: ['name', 'lastActivityDate']
                }
              }
    //console.log("getMapQuery", res)
    return res
  }

  afterGetMapPages(dataRes){
    let date1D = new Date()
    date1D.setMinutes(date1D.getMinutes() - 3);

    

    let finalRes = []
    dataRes.forEach((page)=>{
      page.online = (page.dateConnected > date1D) 
      page.dateConnected = (page.dateConnected!=null) 
                         ? new Date(page.dateConnected) 
                         : new Date("02/01/2020")
      finalRes.push(page)
    })
    
    let sortByDate = (array, key) => {
      let fRes = []
      array.forEach((obj1) => {
          if(fRes.length > 0){
              let spliced = false
              fRes.map((obj2, i) => {
                  let d1 = new Date(obj1[key])
                  let d2 = new Date(obj2[key])
                  if(d1 < d2 && !spliced){
                      fRes.splice(i, 0, obj1)
                      spliced = true
                  }
              })
              if(!spliced) fRes.push(obj1)
          }else{ 
              fRes.push(obj1)
          }
      })
      return fRes
    }

    let sortedRes = sortByDate(finalRes, 'dateConnected').reverse()
     //console.log("**** sortedRes ?", sortedRes.length)
     sortedRes.forEach((page)=>{
       //console.log("**** res ?", page.dateConnected, page.name)
     })

    return sortedRes
  }

}
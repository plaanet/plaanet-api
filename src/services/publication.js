const Publication = require('../models/Publication');
const Instance = require('../models/Instance');

const UserService = require('./user');
const PageService = require('./page');
const RoomService = require("./room");

const core = require('../core/core');

const { EntityDoesNotExistsError } = require('../core/errors');

module.exports = class PublicationService {

  constructor() {
    this.userService = new UserService();
    this.pageService = new PageService();
  }

  async getFirstPostForPage(pageId) {
    const page = await this.pageService.getPageById(pageId);
    if (!page) {
      throw new EntityDoesNotExistsError('Page does not exists', pageId);
    }

    return Publication
      .findOne({
        isFirst: true,
        author: page._id,
      })
      .exec();
  }

  async getStream(body) {
    let radiusMax = core.radiusMaxPost*1000 //500000 //300km
    let radius = body.radius != null ? body.radius : radiusMax 
    if(radius > radiusMax) radius = radiusMax

    let request = { 
        '$and' : [{
            'coordinates': {
                '$geoWithin': {
                    '$centerSphere' :
                        [[body.lat, body.lng], 
                        radius/1000/6378]
                } 
            }
        },{
            'scopeGeo': {
                '$geoIntersects': {
                    '$geometry' : {
                        'type': 'Point',
                        'coordinates': [body.lng, body.lat]
                    }
                } 
            }
        }],
       
    }

    //si un utilisateur se rend sur sa propre page
    //on supprime la condition géographique == donne accès à tous ses messages
    if(body.pageUid != null && body.pageUid == body.userPageUid){
        request['$and'] = []
    }
    if(body.streamOrigin == "favorites") request['$and'] = []

    let search = body.search
    if(search != "" && search != null){
        let words = search.split(' ')
        //console.log("search.split", search, words)
        let arrayW = []
        words.forEach((w)=>{
            let ww = w.trim()
            //if no # then add #
            ww = (ww.indexOf('#') == -1) ? "#" + ww : ww
            if(ww != "") arrayW.push(new RegExp(".*"+ww, "i"))
        })
        request['$and'].push({ hashtags : {  '$in' : arrayW  } })
        //console.log("# search.split request hashtags", arrayW)
    }   

    if(body.pageUid != null){
        //let page = await Page.findOne( { uid: body.pageUid } )
        var mongoose = require('mongoose'); 
        request['$and'].push({'$or' : [{ 'target.uid' : body.pageUid },
                                        { 'signed' : { '$in': [core.iKey(body.pageUid)] } }
                                    ]})

        //prend les publications geo OU les scopes followers
        request = { '$or' : [request, {
                              '$and' : [{ 'scopeType' : 'followers' },
                                        { 'target.uid': body.pageUid }]
                              },{
                              '$and' : [{ 'scopeType' : 'followers' },
                                        { 'signed': { '$in': [core.iKey(body.pageUid)] }}]
                            }]
                  }
    }
   
    if(body.streamOrigin == "favorites"){
        request['$and'].push({ 'favorites': { '$in': body.userPageUid }})
    }
    if(body.streamOrigin == "follows"){
        let i = await Instance.findOne({ isLocal:true })
        let fPages = body.follows //[]
        let fPagesIds = []
        body.follows.forEach((pageUid) => {
            if(core.iName(pageUid) == i.name) //fPages.push(core.iKey(pageUid))
            fPagesIds.push(core.iKey(pageUid))
        })
        //console.log("follows list", fPages)
        request['$and'].push({'$or' : [{ 'target.uid' : { '$in': fPages } },
                                        { 'signed' : { '$in': fPagesIds } }
                                    ]})
    }

    //console.log("request", request)
    let nbPerPage = body.nbPerPage != null ? body.nbPerPage : 5
    let stream = await Publication.find(request)
                        .sort( { created: -1 } )
                        .skip( body.pageNumber > 0 ? ( ( body.pageNumber - 1 ) * nbPerPage ) : 0 )
                        .limit(nbPerPage)
                        .populate({ path: 'author',
                                    select: ['_id', 'uid', 'name', 'city', 'type', 'slug', 'roles']
                        })
                        .populate({ path: 'signed',
                                    select: ['_id', 'uid', 'name', 'city', 'type', 'slug', 'roles']
                        })
                        // .populate({ path: 'target',
                        //             select: ['_id', 'uid', 'name', 'city', 'type', 'slug']
                        // })
                        .populate("pdf")
                        .populate({
                            path: 'comments',
                            options: {
                                sort: 'created',
                                //limit: 3,
                            },
                            populate: { 
                                path: 'answers',
                                options: {
                                   sort: 'created'
                                } 
                            }
                        })

    /* récupération des publications partagée (et des sondages) */
    let countRes = 1
    let localNode = await Instance.findOne({ isLocal:true })
    await Promise.all(stream.map(async(post, i)=>{
        post.views = post.views+1
        post.save()
        
        if(post.sharedPostUid != null){
            let nodePath = await core.getNodePath(post.sharedPostUid)
            let nodePathLo = await core.getNodePath(localNode.name+":")
            //console.log("path ???", nodePath, nodePathLo)
            if(nodePath == nodePathLo){
                let pShared = await Publication.findOne({ uid: post.sharedPostUid })
                                                    .populate("signed")
                                                    .populate({ path: 'author',
                                                                select: ['_id', 'uid', 'name', 'city', 'type', 'slug']
                                                    })
                if(pShared != null && pShared.sharedSurveyUid != null){
                    const roomService = new RoomService()
                    let survey = await roomService.getSurvey(pShared.sharedSurveyUid, body.userPageCoordinates)
                    if(survey != false) pShared.surveyShared = survey
                    //console.log("res survey", survey)
                }
                post.postShared = pShared
                //console.log("res Publication.findOne({ uid: post.sharedPostUid })", post.postShared)
            }else{
                //console.log("try contact", nodePath + '/broadcast/publication-get-post', post.sharedPostUid)
                core.broadcaster.init()
                try{
                    let postRes = await core.asyncPost(
                                    nodePath + '/broadcast/publication-get-post', 
                                    { postUid : post.sharedPostUid,
                                    userPageCoordinates : body.userPageCoordinates })

                    //console.log("res /broadcast/publication-get-post", postRes)
                    if(postRes.data.error == false)
                        post.postShared = postRes.data.postObj
                }catch(e){
                    console.log("res error /broadcast/publication-get-post", e)
                    
                }
            }
        }
        stream[i].postShared = post.postShared

        if(post.sharedSurveyUid != null){
            const roomService = new RoomService()
            let survey = await roomService.getSurvey(post.sharedSurveyUid, body.userPageCoordinates)
            if(survey != false) post.surveyShared = survey
            stream[i].surveyShared = post.surveyShared
        }

        // if(countRes >= stream.length){
        //     console.log("end recup all post shared", stream.length)
        //     let r = { error: false, stream: stream, nbPerPage: nbPerPage }
        //     return r
        // }
        // countRes++
    }))
    /* récupération des publications partagée */

    // if(stream.length == 0){
    //   console.log("end 2 recup all post shared", stream.length)
    //   let r = { error: false, stream: stream, nbPerPage: nbPerPage }
    //   return r
    // }

    //console.log("end 3 getStream", stream.length)
    //let r = 
    return { error: false, stream: stream, nbPerPage: nbPerPage }
  }

  sortByDate(array, key){
    let fRes = []
    array.forEach((obj1) => {
        if(fRes.length > 0){
            let spliced = false
            fRes.map((obj2, i) => {
                let d1 = new Date(obj1[key])
                let d2 = new Date(obj2[key])
                if(d1 < d2 && !spliced){
                    fRes.splice(i, 0, obj1)
                    spliced = true
                }
            })
            if(!spliced) fRes.push(obj1)
        }else{ 
            fRes.push(obj1)
        }
    })
    return fRes
  }
}
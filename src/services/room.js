//const Room = require('../models/Room');

//const UserService = require('./user');
//const PageService = require('./page');
//const { EntityDoesNotExistsError } = require('../core/errors');
const haversine = require("haversine-distance");

const Publication = require('../models/Publication')
const core = require('../core/core');

module.exports = class RoomService {

  constructor() {
    //this.userService = new UserService();
    //this.pageService = new PageService();
  }

  async getSurvey(surveyUid, userCoordinates){
    let nodePath = await core.getNodePath(surveyUid)
    //console.log("try contact", nodePath + '/broadcast/get-survey', surveyUid)
    core.broadcaster.init()
    let surveyRes = await core.asyncPost(
                    nodePath + '/broadcast/get-survey', 
                    { surveyUid : surveyUid, userPage: { coordinates: userCoordinates }})

    if(surveyRes.data.error == false)
        return surveyRes.data.survey
    else return false
  }

  async getSupports(multichain, surveyUid) {
    try{
      let supp = await multichain.listStreamQueryItems({ 
                stream: 'support', 
                query : { key: surveyUid } 
      })
      let supports = [] 
      supp.forEach((support) => { supports.push(support.data.json) })  
      return supports
    }catch(e){
      console.log("cannot connect to multichain")
      return []
    }
  }

  async getVotes(multichain, survey) {
    //console.log("ROOM SERVICE getVotes", survey.uid)
    let v = []
    try{
      v = await multichain.listStreamQueryItems({ 
                  stream: 'vote', 
                  query : { key: survey.uid } 
      })
    }catch(e){
      console.log("cannot connect to multichain")
    }
    
    let votes = [] 
    let results = [] 
    v.forEach((vote) => { 
      //console.log("ROOM SERVICE getVotes 3", vote.data.json)
      votes.push(vote.data.json) 
      if(results[vote.data.json.voteValue] == null) results[vote.data.json.voteValue] = 1
      else results[vote.data.json.voteValue] += 1
    })  

    let resultsArr = []
    if(survey.answers != null)
    survey.answers.forEach((answer, j) => {
        //console.log("results[j]", j, results[j])
        resultsArr[j] = (results[j] == null) ? 0 : results[j]
    })
    

    //console.log("ROOM SERVICE getVotes final")
    return { votes: votes, results: resultsArr }
  }

  async hasAlreadySupport(multichain, surveyUid, userPageUid) {
    let supp = await multichain.listStreamQueryItems({ 
              stream: 'support', 
              query : { key: surveyUid } 
    })
    let alreadySupport = false
    supp.forEach((s)=>{
        if(s.data.json.authorPageUid == userPageUid) alreadySupport = true
    })
    return alreadySupport
  }

  async hasAlreadyVote(multichain, surveyUid, userPageUid) {
    let supp = await multichain.listStreamQueryItems({ 
              stream: 'vote', 
              query : { key: surveyUid } 
    })
    let alreadyVote = false
    supp.forEach((s)=>{
        if(s.data.json.authorPageUid == userPageUid) alreadyVote = true
    })
    return alreadyVote
  }

  
  async openVote(multichain, survey) {
    //ouvre les votes
    survey.open = true

    let now = new Date()
    survey.dateOpen = now

    let dateEndVote = new Date()
    dateEndVote.setDate(now.getDate() + survey.duration)
    survey.dateEnd = dateEndVote    
    survey.save()

    console.log("OPEN VOTE", now, dateEndVote, survey)

    //récupère tous les survey de l'assemblée
    let surveys = await multichain.listStreamQueryItems({ 
        stream: 'survey', 
        query : { key: survey.roomUid } 
    })
    
    //vérifie si le sondage a déjà été ouvert
    let exist = false
    surveys.forEach(s => {
      if(s.data.json.uid == survey.uid) exist=true
    })

    //publie le survey dans la blockchain
    if(exist == false){
      await multichain.publish({  stream: 'survey', 
                                  key: survey.roomUid, 
                                  data: { "json": survey },
                                  options: 'offchain'
                                  })
                      .catch((e)=>{
                          console.log("error publish survey:", e)
                          return res.json( { error: true, e: e } );
                      })
      
      //change la date de publication du post qui contient le survey
      //pour le faire remonter dans le fil d'actualité
      //autre option : publier un nouveau post ? risque de créer des pb
      let post = await Publication.findOne( { uid: survey.postUid } )
      post.created = new Date()
      post.save()
      console.log("open vote on survey", survey.uid)
    }
    
    return true
  }

  canVote(survey, coordinates){
    if(coordinates == null) return false
    let p1 = { lat: survey.parentPage.coordinates[0], lng: survey.parentPage.coordinates[1] }
    let p2 = { lat: coordinates[0], lng: coordinates[1] }
    var haversine_m = haversine(p1, p2)
    //console.log("canVote 200", haversine_m)
    return (haversine_m / 1000) < 230
  }

  canSurvey(page, coordinates){
    let p1 = { lat: page.coordinates[0], lng: page.coordinates[1] }
    let p2 = { lat: coordinates[0], lng: coordinates[1] }
    var haversine_m = haversine(p1, p2)
    //console.log("canSurvey 200", haversine_m)
    return (haversine_m / 1000) < 230
  }

  orderBy(surveys, filter){
    if(filter == 'startedSoon') {
        surveys.sort(function(a, b) {
            var c = new Date(a.dateOpen)
            var d = new Date(b.dateOpen)
            return c-d;
        })
    }
    
    if(filter == 'endSoon') {
        surveys.sort(function(a, b) {
            var c = new Date(a.dateEnd)
            var d = new Date(b.dateEnd)
            return c-d;
        })
    }

    if(filter == 'nbVotes') {
        surveys.sort(function(a, b) {
            return b.nbVotes - a.nbVotes
        })
    }
    return surveys
  }
}
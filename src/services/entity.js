const Instance = require('../models/Instance');
const StatUrl = require('../models/StatUrl');

const fs = require('fs')
const core = require('../core/core');

module.exports = class EntityService {

  constructor() {}

  requireModel(entityType){
    //remplace la premiere lettre par une majuscule
    let entityName = entityType.replace(/^\w/, (c) => c.toUpperCase());
    return require('../models/' + entityName)
  }

  async getEntities(entityType, qry, after=()=>{ return [] }) {
    //console.log("getEntities query", query)
    let params = {  entityType : entityType,
                    query: qry.query,
                    limit: qry.limit,
                    skip: qry.skip,
                    sort: qry.sort,
                    populate: qry.populate
                  }
                  
    try{ //broadcast request (to each instance of the network)
      let fRes = await core.broadcaster.fast('/fast/get-entities', params, //params
        async (foreignEntities) => { // == then (after response from all instances)
          //if network sent data
          if(foreignEntities != null){
            const Model = this.requireModel(entityType)
            let count = await Model.countDocuments(params.query)
            let localEntities = await this.queryEntity(params)
            let allRes = foreignEntities.concat(localEntities)
            let finalRes = after(allRes)

            return {  error: false, 
                      entities: finalRes, 
                      count: count,
                      limit: qry.limit,
                      skip: qry.skip }
          }else{
              return { error: true, entities: null, 
                        errorMsg: 'No ' + entityType + ' found in this bdd' }
          }
        },
        function(result){ // == catch
          console.log("getEntities error 2 : fCatch", result)
          res.json({ error: true, result: result })
        },  
        'entities', true) //foreignEntities[id] to concat for each response after forward
    
      return fRes
    }
    catch(e){
        console.log("getEntities error 3", e)
        res.json({ error: true, entities: null, e:e })
    }
  }

  async queryEntity(qry){
    let limit = qry.limit ? qry.limit : 100
    limit = limit > 100 ? 100 : limit

    const Model = this.requireModel(qry.entityType)
    //console.log("queryEntity", qry)
    //console.log("$and", qry.query['$and'])
    //console.log("$or", qry.query['$or'])
    try{
      let select = ["-accessToken", "-password", "-email"] //ne jamais renvoyer les token, ni les mot de passe
      let entities = await Model.find(qry.query)
                                .select(select)
                                .sort(qry.sort)
                                .skip(qry.skip*limit)
                                .limit(limit)
                                .populate(qry.populate)
      return entities
    }catch(e){
      console.log("***************error", e)
      return []
    }
  }

}
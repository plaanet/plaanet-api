const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

const Config = require('../core/config');
const { EntityDoesNotExistsError } = require('../core/errors');

const { User } = require('../models/User');

module.exports = class UserService {

  constructor() { }

  async countUsers() {
    return User
      .countDocuments();
  }

  async getMyUserById(id) {
    return User
      .findById(mongoose.Types.ObjectId(id))
      .select('-password')
      .populate('pages')
      .exec();
  }
  async getUserById(id) {
    return User
      .findById(mongoose.Types.ObjectId(id))
      .select('-password -email')
      .populate('pages')
      .exec();
  }

  async unsetEmailById(id) {
    User.updateOne({_id: id}, {$unset:  {lastDailyEmailDate: 1 }}).exec();
    User.updateOne({_id: id}, {$unset:  {allowEmail: 1 }}).exec();
    return User.updateOne({_id: id}, {$unset:  {email: 1 }}).exec();
  }

  async initEmailDelivery() {
    let date1D = new Date()
    date1D.setDate(date1D.getDate() - 2)
    
    let all = await User.find({ allowEmail: { '$exists' : true } }).limit(150)
    console.log("initEmailDelivery", all.length)

    await Promise.all(all.map(async (user, u)=>{ 
      // user.lastDailyEmailDate = date1D
      // user.email = user.name + "@mail.com"
      // user.allowEmail = true
      // await user.save()
      await this.unsetEmailById(user._id)
    }))
    return true
  }

  async getUserForMailing(limit) {
    let date1D = new Date()
    date1D.setDate(date1D.getDate() - 1)
    return User.find({ email: { '$exists' : true },
                       allowEmail: true,
                       lastDailyEmailDate: { '$lt' : date1D }, 
                    })
              .select('name email pages lastDailyEmailDate')
              .populate('pages', '_id uid dateReadNotif')
              .sort({'lastDailyEmailDate':-1})
              .limit(limit)
  }

  async getUserByIdWithPassword(id) {
    return User
      .findById(mongoose.Types.ObjectId(id)).select('-email')
      .exec();
  }

  async getRedactedUserById(id) {
    return User
      .findById(mongoose.Types.ObjectId(id))
      .select('_id name pages')
      // Only populate profil page
      .populate('pages', '_id uid slug', null, { type: 'user', owner: mongoose.Types.ObjectId(id) })
      .exec();
  }

  async getUserByName(name) {
    return User
      .findOne({ name })
      .select('-password -email')
      .exec();
  }

  async compareUserPassword(id, password) {
    const user = await this.getUserByIdWithPassword(id);
    if (!user) {
      throw new EntityDoesNotExistsError('User does not exists.', id);
    }

    return bcrypt.compare(password, user.password);
  }

  async generateTokenForUser(id) {
    const user = await this.getUserById(id);
    if (!user) {
      throw new EntityDoesNotExistsError('User does not exists.', id);
    }

    const payload = {
      _id: id,
      username: user.name,
      role: user.isAdmin ? 'admin' : 'user',
    };

    return jwt.sign(payload, Config.Secrets.COOKIES_KEY);
  }

  async isUserAdmin(id) {
    const user = await this.getUserById(id);

    if (user.isAdmin === true) {
      return true;
    }

    return false;
  }

  async setLastActivity(id, date) {
    const user = await this.getUserById(id);
    user.lastActivityDate = date;
    await user.save();
  }

  sortByDate(array, key){
    let fRes = []
    array.forEach((obj1) => {
        if(fRes.length > 0){
            let spliced = false
            fRes.map((obj2, i) => {
                let d1 = new Date(obj1[key])
                let d2 = new Date(obj2[key])
                if(d1 < d2 && !spliced){
                    fRes.splice(i, 0, obj1)
                    spliced = true
                }
            })
            if(!spliced) fRes.push(obj1)
        }else{ 
            fRes.push(obj1)
        }
    })
    return fRes
  }

  userToJson(user){ 
    return {
      _id: user._id,
      name: user.name,
      email: user.email,
      pages: user.pages,
      isActive: user.isActive,
      isAdmin: user.isAdmin,
      lastActivityDate: user.lastActivityDate,
      lastDailyEmailDate: user.lastDailyEmailDate,
      allowEmail: user.allowEmail,
    }
  }

}
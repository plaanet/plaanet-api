const express = require('express');
const config = require('config');

const auth = require('../core/middleware/auth');

const MailService = require('../services/mail');

const UserService = require('../services/user');
const NotifService = require('../services/notification');
const PageService = require('../services/page');

const mailsDailyNotif = require('../mails/daily_notif.js');

const router = express.Router()

router.get('/send-mail-test', async (req, res) => {
  console.log("-------------------------")
  console.log("/mail/send-mail-test")
  
  if(config.get("env") != 'dev') 
    res.json({ msg: "sorry, you are not in dev env" })

  let mailService = new MailService()
  let html = "<b>It works !</b>"
  html = mailsDailyNotif({ name: "SuperAdmin Test", nbNotifTotal: 8 })

  let test = true
  if(test){ //en mode test : affiche le mail à l'écran sans l'envoyer
    res.set('Content-Type', 'text/html');
    res.send(Buffer.from("<body>" + html + "</body>"));
    return
  }
  else{ //si pas mode test : envoie le mail
    let resMail = await mailService.sendMail(config.get("mail_email_test"), "Test email", html)
  }

  
  res.json({ "result": resMail })
})


router.get('/dailymail/:cronToken/:test?', async (req, res) => {
  console.log("-------------------------")
  console.log("/mail/dailymail", req.params)
  
  // if(config.get("env") != 'dev') 
  //   return res.json({ msg: "sorry, you are not in dev env" })

  if(req.params.cronToken != config.get("cron_pk"))
    return res.json({ msg: "sorry, your token is not valid" })

  let mailService = new MailService()
  let userService = new UserService()
  let pageService = new PageService()
  let notifService = new NotifService()

  //await userService.initEmailDelivery()
  let { limit } = await mailService.getCurrentLimitFor24h()
  console.log("limit", limit) 

  //récupère la liste des users qui ont fourni un email
  let users = await userService.getUserForMailing(limit)

  //initialise la liste des mails à envoyer
  let toNotifByMail = []
  //pour tous les users qui ont fourni un email
  await Promise.all(users.map(async (user, u)=>{
    let data = await mailService.getNotifMailData(user)
    //enregistre les détails du mail à envoyer 
    toNotifByMail.push(data)
  }))

  if(req.params.test != 'test'){
    await Promise.all(toNotifByMail.map(async (mail, m)=>{
      if(mail.nbNotifTotal > 0){
        let r = await mailService.sendDailyNotif(mail)
        toNotifByMail[m].sent = r.success
      }
      await mailService.traceDailyEmailChecked(mail.userId)
    }))
  }else{
    console.log(">>>", toNotifByMail.length, "dailymail à envoyer")
    console.log(toNotifByMail)
    console.log("<<<", toNotifByMail.length, "dailymail à envoyer")

    let html = ""
    toNotifByMail.forEach((mail)=>{ 
      //if(mail.nbNotifTotal > 0) 
        html += mail.email + "<br>" + mailsDailyNotif(mail) + "<br>" 
    })
    
    if(toNotifByMail.length == 0) html = "Aucun mail à envoyer"

    res.set('Content-Type', 'text/html');
    res.send(Buffer.from(html));
    return
  }

  res.json({ "success": true, "result": toNotifByMail })
})


module.exports = router;
var express = require("express")
var router = express.Router()
const config = require('config');

const auth = require("../core/middleware/auth")
//const PageService = require("../services/page");

// const { User, validate } = require('../models/User')
// const Instance = require('../models/Instance')
// const Publication = require('../models/Publication')
// const Notification = require('../models/Notification')
// const Page = require('../models/Page')
const core = require('../core/core')
const Instance = require('../models/Instance')
const Page = require('../models/Page')
const Publication = require('../models/Publication')
const Survey = require('../models/Survey')
const RoomService = require("../services/room");


let multichain = require("multinodejs")({
  port: config.multichain.port,
  host: config.multichain.host,
  user: config.multichain.user,
  pass: config.multichain.pass
});

//to send a new survey
router.post('/save-survey', auth, async (req, res) => {
  console.log("-------------------------")
  console.log("/room/save-survey")

  try{ //survey a save on the server of the room
      let nodePath = await core.getNodePath(req.body.roomUid)
      let userPage = await Page.findOne({ owner: req.user._id, type: 'user' })

      let url = '/broadcast/save-survey'
      if(req.body.typeEdition == 'edit') url = '/broadcast/edit-survey'

      console.log("try contact", nodePath + url)
      core.post(nodePath + url, 
          {   roomUid : req.body.roomUid,
              type: req.body.type,
              title: req.body.title,
              text: req.body.text,
              tags: req.body.tags,
              answers: req.body.answers,
              duration: req.body.duration,
              authorPage: { uid : userPage.uid, 
                            name: userPage.name, 
                            city: userPage.city , 
                            coordinates: userPage.coordinates 
              },
              originalSurveyUid: req.body.originalSurveyUid,
              typeEdition: req.body.typeEdition
  
          }, //params
          function(dataRes){ // == then (after response from all instances)
              //console.log("dataRes", dataRes.data)
              res.json(dataRes.data)                    
          },function(result){ // == catch
              console.log("error 2 : fCatch", result)
          })
  }catch(e){
      console.log("error 3")
      res.json({ error: true, e:e })
  }
})

//to duplicate a new survey
router.post('/duplicate-survey', auth, async (req, res) => {
    console.log("-------------------------")
    console.log("/room/duplicate-survey", req.body)

    let userPage = await Page.findOne({ owner: req.user._id, type: 'user' })
    let nodePath = await core.getNodePath(req.body.inRoomUid)

    const roomService = new RoomService()
    let survey = await roomService.getSurvey(req.body.surveyUid, userPage.coordinates)

    if(survey == null) 
        return res.json( { error: true, msg: 'survey to duplicate not found' } )
  
    try{ //broadcast request (to each instance of the network)
        console.log("try contact", nodePath + '/broadcast/save-survey')
        core.post(nodePath + '/broadcast/save-survey', 
            {   roomUid : req.body.inRoomUid,
                type: survey.type,
                title: survey.title,
                text: survey.text,
                tags: survey.tags,
                answers: survey.answers,
                duration: survey.duration,
                authorPage: {   uid : userPage.uid, 
                                name: userPage.name, 
                                city: userPage.city , 
                                coordinates: userPage.coordinates 
                },
                originalSurveyUid: survey.originalSurveyUid,
                typeEdition: req.body.typeEdition    
            }, //params
            function(dataRes){ // == then (after response from all instances)
                //console.log("dataRes", dataRes.data)
                res.json(dataRes.data)                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            })
    }catch(e){
        console.log("error 3")
        res.json({ error: true, e:e })
    }
})

//to delete a new survey
router.post('/delete-survey', auth, async (req, res) => {
    console.log("-------------------------")
    console.log("/room/delete-survey", req.body)
  
    try{ //broadcast request (to each instance of the network)
        let nodePath = await core.getNodePath(req.body.surveyUid)
        let userPage = await Page.findOne({ owner: req.user._id, type: 'user' }).populate("owner")
        console.log("try contact", nodePath + '/broadcast/delete-survey')
        core.post(nodePath + '/broadcast/delete-survey', 
            {   surveyUid : req.body.surveyUid,
                userPageUid: userPage.uid,
                userPageIsAdmin: userPage.owner.isAdmin
            }, //params
            function(dataRes){ // == then (after response from all instances)
                //console.log("dataRes", dataRes.data)
                res.json(dataRes.data)                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            })
    }catch(e){
        console.log("error 3")
        res.json({ error: true, e:e })
    }
})

   
//allow users to vote
router.get('/open-vote',  async (req, res) => {
    console.log("-------------------------")
    console.log("/room/open-vote")
  
    let date1D = new Date()
    date1D.setDate(date1D.getDate() - 1)
    //récupère les sondages qui sont fermé, et qui ont été créé il y a plus de 24h (1 jour)
    //good request : created: { $lt: date1D } - less than
    let surveys = await Survey.find( { created: { $lt: date1D }, 
                                        dateOpen: null,
                                        open: false, 
                                        rejected: false } )
  
    let nbSurveyOpen = 0
    let nbSurveyClose = 0
    const roomService = new RoomService()
    console.log("/room/open-vote2", surveys.length)
    try{
      //pour chaque sondage
      await Promise.all(surveys.map(async (survey, i) => {
          let supports = await roomService.getSupports(multichain, survey.uid)
          let supp0 = 0
          let supp1 = 0
          //calcul les résultats des votes de soutient
          supports.forEach(s => {
              if(s.voteValue == "0") supp0++
              if(s.voteValue == "1") supp1++
          })
  
          console.log("results", supp1, supp0)
          //si + de 50% de soutient : ouvre les votes
          if(supp1 > supp0){
              await roomService.openVote(multichain, survey)
              nbSurveyOpen++
          }else{
              //supprime le post qui contient le sondage
              await Publication.deleteOne( { sharedSurveyUid: survey.uid } )
              survey.rejected = true
              survey.save()
              nbSurveyClose++
          }
      }))
      }catch(e){
          console.log("error 3")
          res.json({ error: true, e:e })
      } 
      
      res.json({  error: false, 
                  nbSurveyOpen: nbSurveyOpen, 
                  nbSurveyClose: nbSurveyClose })
  })
  
   
//allow users to vote
router.get('/close-vote',  async (req, res) => {
    console.log("-------------------------")
    console.log("/room/close-vote")
  
    let now = new Date()
    //récupère les sondages dont la date de fin a été dépassée
    let surveys = await Survey.find( {  dateEnd: { $lt: now }, 
                                        open: true, rejected: false } )
    try{
      //pour chaque sondage
      await Promise.all(surveys.map(async (survey, i) => {
        //fait remonter le post qui contient le sondage dans le stream
        let post = await Publication.findOne( { sharedSurveyUid: survey.uid } )
        post.created = now
        post.save()

        //ferme les votes
        survey.open = false
        survey.save()
      }))
    }catch(e){
        console.log("catch error in /room/close-vote")
        res.json({ error: true, e:e })
    } 
      
    res.json({  error: false, 
                surveys: surveys.length })
  })
  
    
router.post('/get-surveys', auth,  async (req, res) => {
  console.log("-------------------------")
  console.log("/room/get-surveys", req.body)

  try{
    let userPage = await Page.findOne({ owner: req.user._id, type: 'user' })
    let roomUid = req.body.roomUid
    if(roomUid == null) roomUid = userPage.uid
    
    let nodePath = await core.getNodePath(roomUid)

    let params =  {   roomUid : req.body.roomUid,
                        filter: req.body.filter,
                        searchStr: req.body.searchStr,
                        searchType: req.body.searchType,
                        userPage: { uid : userPage.uid, 
                                    //name: userPage.name, 
                                    //city: userPage.city, 
                                    coordinates: userPage.coordinates 
                    }
    }

    if(req.body.roomUid != null){
        //console.log("try contact", nodePath + '/broadcast/get-surveys')
        core.post(nodePath + '/broadcast/get-surveys', 
            params, //params
            function(dataRes){ // == then (after response from all instances)
                //console.log("dataRes", dataRes.data)
                res.json(dataRes.data)                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            })
    
    }else{    
        //console.log("try contact by broadcast /broadcast/get-surveys")
        core.broadcaster.post('/broadcast/get-surveys', 
            params, //params
            function(dataRes){ // == then (after response from all instances)
                //console.log("dataRes /broadcast/get-surveys", dataRes.length)
                res.json({ error: false, surveys: dataRes})                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            }, 'surveys')
    }
  }catch(e){
      console.log("error 3", e)
      res.json({ error: true, e:e })
  }
})


router.get('/get-survey/:surveyUid', auth,  async (req, res) => {
  console.log("-------------------------")
  console.log("/room/get-survey", req.params)

  try{ //broadcast request (to each instance of the network)
    let nodePath = await core.getNodePath(req.params.surveyUid)
    let userPage = await Page.findOne({ owner: req.user._id, type: 'user' })
    
    //console.log("try contact", nodePath + '/broadcast/get-survey')
    core.post(nodePath + '/broadcast/get-survey', 
        {   surveyUid : req.params.surveyUid,
            userPage: { uid : userPage.uid, 
                        //name: userPage.name, 
                        //city: userPage.city , 
                        coordinates: userPage.coordinates 
            }
        }, //params
        function(dataRes){ // == then (after response from all instances)
            //console.log("dataRes", dataRes.data)
            res.json(dataRes.data)                    
        },function(result){ // == catch
            console.log("error 2 : fCatch", result)
        })
  }catch(e){
      console.log("error 3")
      res.json({ error: true, e:e })
  }
})

router.post('/send-support', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/room/send-support", req.body)
  
    try{ //broadcast request (to each instance of the network)
      let nodePath = await core.getNodePath(req.body.surveyUid)
      let userPage = await Page.findOne({ owner: req.user._id, type: 'user' })
      
      console.log("try contact", nodePath + '/broadcast/send-support')
      core.post(nodePath + '/broadcast/send-support', 
          { surveyUid : req.body.surveyUid,
            voteValue : req.body.voteValue,
            userPage: { uid : userPage.uid, 
                        //name: userPage.name, 
                        //city: userPage.city,
                        coordinates: userPage.coordinates  
            }
          }, //params
          function(dataRes){ // == then (after response from all instances)
              //console.log("dataRes", dataRes.data)
              res.json(dataRes.data)                    
          },function(result){ // == catch
              console.log("error 2 : fCatch", result)
          })
    }catch(e){
        console.log("error 3")
        res.json({ error: true, e:e })
    }
  })

  router.post('/send-vote', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/room/send-vote", req.body)
  
    try{ //broadcast request (to each instance of the network)
      let nodePath = await core.getNodePath(req.body.surveyUid)
      let userPage = await Page.findOne({ owner: req.user._id, type: 'user' })
      
      console.log("try contact", nodePath + '/broadcast/send-vote')
      core.post(nodePath + '/broadcast/send-vote', 
          { surveyUid : req.body.surveyUid,
            voteValue : req.body.voteValue,
            userPage: { uid : userPage.uid, 
                        //name: userPage.name, 
                        //city: userPage.city,
                        coordinates: userPage.coordinates  
            }
          }, //params
          function(dataRes){ // == then (after response from all instances)
              //console.log("dataRes", dataRes.data)
              res.json(dataRes.data)                    
          },function(result){ // == catch
              console.log("error 2 : fCatch", result)
          })
    }catch(e){
        console.log("error 3")
        res.json({ error: true, e:e })
    }
  })



  router.get('/get-cumul-results/:surveyUid', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/room/get-cumul-results", req.params)
  
    try{ //broadcast request (to each instance of the network)
      let userPage = await Page.findOne({ owner: req.user._id, type: 'user' })
      
      core.broadcaster.post('/broadcast/get-cumul-results', 
          {   surveyUid : req.params.surveyUid,
              userPage: { uid : userPage.uid, 
                          coordinates: userPage.coordinates 
              }
          }, //params
          function(dataRes){ // == then (after response from all instances)
              //console.log("dataRes nb:", dataRes.length)

              res.json({ error: false, surveys: dataRes })                    
          },function(result){ // == catch
              console.log("error 2 : fCatch", result)
          },
          'surveys')
    }catch(e){
        console.log("error 3")
        res.json({ error: true, e:e })
    }
  })
  
 

module.exports = router;
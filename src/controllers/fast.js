var express = require("express")
var router = express.Router()

const auth = require("../core/middleware/auth-node");
const EntityService = require("../services/entity");

router.get('/ready', async (req, res) => {
  return res.send({ ready: true })
})

router.post('/get-entities', auth,  async (req, res) => {
  //console.log("--------------------------------")
  //console.log("/fast/get-entities", req.body.entityType, req.body.limit, req.body.skip, req.body.sort)

  if(req.body.entityType == null)
    return res.send({ error: true })
  
  let entityService = new EntityService()
  let entities = await entityService.queryEntity(req.body)
                                              
  //console.log("entities found:", entities.length)
  return res.send({ error: false, entities: entities })
})


module.exports = router;
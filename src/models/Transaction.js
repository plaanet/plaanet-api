var mongoose = require('mongoose');

var transactionSchema = new mongoose.Schema({
    amount: Number,
    label: String,
    transKey: String,
    senderUid: String,
    receiverUid: String,
    senderName: String,
    receiverName: String,
    senderUni: Number,
    receiverUni: Number,
    status: String,
    created: Date,
    validated: Date,
});


transactionSchema.virtual('transactions', {
    ref: 'User',
    localField: '_id',
    foreignField: 'pendingTransactions'
});

transactionSchema.virtual('transactions', {
    ref: 'User',
    localField: '_id',
    foreignField: 'validatedTransactions'
});


var Transaction = mongoose.model('Transaction', transactionSchema);

module.exports = Transaction;
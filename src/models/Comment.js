var mongoose = require("mongoose")

var commentSchema = new mongoose.Schema({
    uid: String,
    text: String,
    avatar: String,
    emojis: [{
        name: String,
        users: [ String ]
    }] ,
    
    //Array, 
    /*   [{  "name": "emoticon-lol", 
                            "subscribers" : [ userId, userId, ... ] }
                        ]*/
    parentPost: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Publication'
    },
    parentComment: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment'
    },
    answers: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment'
    }],
    author: mongoose.Schema.Types.Mixed, //: { uid, name, avatar }
    created: Date,
})

commentSchema.virtual('comments', {
    ref: 'Publication',
    localField: '_id',
    foreignField: 'comments'
});

commentSchema.virtual('allAnswers', {
    ref: 'Comment',
    localField: '_id',
    foreignField: 'answers'
});

var Comment = mongoose.model('Comment', commentSchema)
module.exports = Comment;

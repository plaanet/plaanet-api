var mongoose = require("mongoose")

var notificationSchema = new mongoose.Schema({
    //uid: String,
    verb: String,
    created: Date,
    updated: Date,
    whatObj: mongoose.Schema.Types.Mixed,
    aboutObj: mongoose.Schema.Types.Mixed,
    authors: Array,
    targets: Array,
})

var Notification = mongoose.model('Notification', notificationSchema)
module.exports = Notification;

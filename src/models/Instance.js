var mongoose = require("mongoose")

var instanceSchema = new mongoose.Schema({
    name: String,
    nodeKey: String,
    ip: String,
    url: String,
    port: String,
    portSocket: String,
    city: String,
    state: String,
    position: mongoose.Schema.Types.Mixed,
    clientUrl: String,
    clientPort: String,
    nbUsers: Number,
    nbPosts: Number,
    nbNotifs: Number,
    nbUserGlobal: Number,
    nbMobileUsers: Number,
    env: String,
    isLocal: Boolean,
    isActive: {
        type: Boolean,
        default: true,
    },
    unreachable: {
        type: Boolean,
        default: false,
    },
    lockOut: {              //Forbid local instance to send request out, to an other instance
        type: Boolean,
        default: false,
    },       
    lockIn: {               //Forbid local instance to respond to request from other instances
        type: Boolean,
        default: false,
    },           
    lockBy: {               //If local instance is lockin by this instance
        type: Boolean,
        default: false,
    },   
    admins: [mongoose.Schema.Types.Mixed],     
    dateInfoUpdated: Date,
})


var Instance = mongoose.model('Instance', instanceSchema)

Instance.calcUniValue = function(mm, nbUser){
    return mm / nbUser / 365.25 * 12
}

module.exports = Instance;

var mongoose = require("mongoose")

var mobileAppSchema = new mongoose.Schema({
    name: String,
    version: String,
    description: String,
    OS: String,
    created: Date,
    updated: Date,
})

var MobileApp = mongoose.model('MobileApp', mobileAppSchema)
module.exports = MobileApp;
